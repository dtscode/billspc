import cmdarg
import ircparser
import asyncnet
import asyncdispatch

proc getHostname(raw: string): string =
    result = raw[(find(raw, '!') + 1) .. raw.high]

proc getNick(raw: string): string =
    result = raw[1 .. (find(raw, '!') - 1)]

when defined(debug):
    proc echoCommand(command: string) =
        debugEcho("::>>> " & command)

var info: connection = process()

connect(info)
asyncCheck send(sock, "foo\r\n")

while true:
    var response = recv()
    response.callback =
        proc() =
            echo(response.read)

runForever()
















































discard """
nick BillsPC
send(sock, "user BillsPC BillsPC BillsPC :BillsPC\r\n")
send(sock, "pass REDACTED\r\n")
send(sock, "server dtscode.io 7000 REDACTED:REDACTED\r\n")

while true:
    readLine(sock, buffer)
    echo(buffer)

    if endsWith(buffer, "Welcome to dtscode.io! Enjoy your stay!"):
        break

for command in startup:
    send(sock, command & "\r\n")

while true:
    if split(buffer, " ")[1] == "PRIVMSG":
        var words: seq[string] = split(buffer, " ")
        lastLineNick[words[2]] = getNick(words[0])
        lastLine[words[2]] = join(words[3 .. words.high], " ")
        lastLine[words[2]] = lastLine[words[2]][1 .. lastLine[words[2]].high]

    readLine(sock, buffer)

    if buffer == "":
        echo("connection is closed")
        break

    var ircmsg = split(buffer, " ")

    write(stdout, (prettify(ircmsg)))
#    echo($ircmsg)

    if ircmsg[0] == "PING":
        send(sock, "pong " & ircmsg[1] & "\r\n")

    elif ircmsg[1] == "352":
        var hostname: string = ircmsg[4] & "@" & ircmsg[5]

        if waitingForWho[0] == true:
            add(allowed, hostname)
            let whitelistHandle = open("whitelist.log", fmAppend)
            write(whitelistHandle, hostname, "\n")
            close(whitelistHandle)
            waitingForWho[0] = false

        else:
            if hostname in allowed:
                del(allowed, find(allowed, hostname))
                let whitelistHandle = open("whitelist.log", fmWrite)

                for person in allowed:
                    writeLn(whitelistHandle, person)

                close(whitelistHandle)
                waitingForWho[1] = false

    # make bot more verbose
    elif ircmsg[1] == "PRIVMSG":
        var hostname: string = getHostname(ircmsg[0])
        var nick: string = getNick(ircmsg[0])

        if ircmsg[3][0 .. 2] == ":s/":
            var words: seq[string] = splitWords(ircmsg[3], '/')

            if len(words) > 2:
                var badWord = words[1]
                var goodWord = words[2]
                lastLine[ircmsg[2]] = re.replace(lastLine[ircmsg[2]], re(badWord), goodWord)
                send(sock, "PRIVMSG " & ircmsg[2] & " :" & lastLineNick[ircmsg[2]] & " meant: " & lastLine[ircmsg[2]] & "\r\n")

        if len(ircmsg) >= 5:
            case ircmsg[3]:
                of ":,add": # fix add and remove to do it by nick
                    if hostname == "~dtscode@dtscode.io":
                        send(sock, "who " & nick & "\r\n")
                        waitingForWho[0] = true
                        echo "out of add"

                of ":,remove":
                    if hostname == "~dtscode@dtscode.io":
                        send(sock, "who " & nick & "\r\n")
                        waitingForWho[1] = true
                        echo "out of remove"

                of ":,showops":
                    var command: string = ""

                    if ircmsg[4] == "at":
                        command = "notice " & ircmsg[5] & " :"

                    else:
                        command = "notice " & ircmsg[4] & " :"

                    for person in allowed:
                        send(sock, command & person & "\r\n")

                of ":,quit":
                    if hostname in allowed:
                        var msg = "quit"

                        if ircmsg.len > 5:
                            msg &= " :" & join(ircmsg[5 .. ircmsg.high], " ")

                        msg &= "\r\n"
                        send(sock, msg)

                of ":,part": # fix the part message
                    if hostname in allowed:
                        var msg = "part " & ircmsg[4]

                        if ircmsg.len > 5:
                            msg &= " :Part: " & join(ircmsg[5 .. ircmsg.high], " ")

                        msg &= "\r\n"
                        send(sock, msg)


                of ":,join":
                    if hostname in allowed:
                        send(sock, "join " & ircmsg[4] & "\r\n")

                of ":,ping", ":,marco":
                    var response = if ircmsg[3] == ":,ping": "pong " & nick
                                   else: "polo"

                    send(sock, "privmsg " & ircmsg[2] & " :" & response & "\r\n")

                of ":,echo":
                    send(sock, "privmsg " & ircmsg[2] & " :" & join(ircmsg[4 .. ircmsg.high], " ") & "\r\n")

                of ":,msg":
                    if hostname in allowed:
                        send(sock, "privmsg " & ircmsg[4] & " :" & join(ircmsg[5 .. ircmsg.high], " ") & "\r\n")
                        send(sock, "notice " & nick & " :Message to " & ircmsg[4] & " has been sent\r\n")

                of ":,raw":
                    if hostname in allowed:
                        send(sock, join(ircmsg[4 .. ircmsg.high], " ") & "\r\n")

                of ":,nick":
                    if hostname in allowed:
                        send(sock, "nick " & ircmsg[4] & "\r\n")

                of ":,me":
                    if hostname in allowed:
                        send(sock, "privmsg " & ircmsg[2] & " :\x01ACTION " & join(ircmsg[4 .. ircmsg.high], " ") & "\x01\r\n")

                of ":,lmgtfy":
                    if ircmsg[4] == "at":
                        send(sock, "privmsg " & ircmsg[2] & " :" & ircmsg[5] & ": http://lmgtfy.com/?q=" & join(ircmsg[6 .. ircmsg.high], "+") & "\r\n") 

                    else:
                        send(sock, "privmsg " & ircmsg[2] & " :" & nick & ": http://lmgtfy.com/?q=" & join(ircmsg[4 .. ircmsg.high], "+") & "\r\n")

                of ":,bash":
                    discard

                of ":,op":
                    if hostname in allowed:
                       send(sock, "cs op " & ircmsg[2] & " " & ircmsg[4] & "\r\n")

                of ":,deop":
                    if hostname in allowed:
                       send(sock, "cs deop " & ircmsg[2] & " " & ircmsg[4] & "\r\n")

                of ":,voice":
                    send(sock, "cs voice " & ircmsg[2] & " " & ircmsg[4] & "\r\n")

                of ":,devoice":
                    send(sock, "cs devoice " & ircmsg[2] & " " & ircmsg[4] & "\r\n")

                else:
                    if hostname in allowed:
                        send(sock, ircmsg[3][2 .. ircmsg[3].high] & " " & join(ircmsg[4 .. ircmsg.high], " ") & "\r\n")
close(sock)
"""
