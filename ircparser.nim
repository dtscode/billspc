import asyncdispatch
import asyncnet

import cmdarg

var sock*: AsyncSocket

proc connect*(info: connection) =
    sock = newAsyncSocket()
    asyncCheck connect(sock, info.server, info.port.Port)

proc recv*(): Future[string] =
    result = recvLine(sock)