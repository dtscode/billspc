import nake
import os

proc create(file: string) =
    open(file, fmWrite).close()

const
    ExeName: string = "billspc"
    BleedingEdgeFeatures: string = "--stackTrace:on --lineTrace:on --threads:off --checks:on --objChecks:on --fieldChecks:on --rangeChecks:on --boundChecks:on --overflowChecks:off --floatChecks:off --assertions:on --nanChecks:off --infChecks:off --deadCodeElim:off --opt:none --app:console --warnings:on --hints:on --debuginfo --debugger:off --verbosity:3"
    ReleaseFeatures: string = "-f --stackTrace:off --lineTrace:off --threads:off --checks:on --objChecks:on --fieldChecks:off --rangeChecks:on --boundChecks:on --overflowChecks:off --floatChecks:off --assertions:off --nanChecks:off --infChecks:off --deadCodeElim:on --opt:speed --app:console --warnings:on --hints:on --debugger:off --parallelBuild:0 --verbosity:3"

task defaultTask, "build app":
    shell(nimExe, "c", BleedingEdgeFeatures, "-d:debug", ExeName)
    create("whitelist.log")
    create("startup.txt")

task "release", "build app":
    shell(nimExe, "c", ReleaseFeatures, "-d:release", ExeName)
    create("whitelist.log")
    create("startup.txt")

task "clean", "clean app":
    removeFile("billspc")
    removeFile("nimcache")
    removeFile("whitelist.log")
    removeFile("startup.txt")
