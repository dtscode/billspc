import parseopt2, parsecfg
import strutils
import streams

type
    connection* = object
        server*: string
        serverPass*: string
        port*: int
        username*: string
        nickname*: string
        password*: string
        channels*: seq[string]
        verbosity*: int
        configFile*: string

proc init(): connection =
    result.server = "irc.freenode.net"
    result.serverPass = ""
    result.port = 6667
    result.username = "BillsPC"
    result.nickname = "BillsPC"
    result.password = ""
    result.channels = @["#cplusplus.com"]
    result.verbosity = 3
    result.configFile = ""

proc process(filename: string, info: var connection) =
    var handle = newFileStream(filename, fmRead)

    if handle == nil:
        return

    var parser: CfgParser
    open(parser, handle, filename)

    while true:
        var event = next(parser)

        case event.kind:
            of cfgEof:
                break

            of cfgSectionStart:
                discard

                                
            of cfgKeyValuePair, cfgOption:
                case event.key:
                    of "server":
                        info.server = event.value

                    of "serverPass":
                        info.serverPass = event.value

                    of "port":
                        info.port = parseInt(event.value)

                    of "username":
                        info.username = event.value

                    of "nickname":
                        info.nickname = event.value

                    of "password":
                        info.password = event.value

                    of "channels":
                        info.channels = split(event.value, ",")

                    of "verbosity":
                        info.verbosity = parseInt(event.value)

            of cfgError:
                write(stderr, event.msg)
    close(parser)

proc process*(): connection =
    result = init()
    
    for kind, key, val in getopt():
        case kind:
            of cmdArgument:
                discard

            of cmdShortOption, cmdLongOption:
                case key:
                    of "server", "s":
                        result.server = val

                    of "serverPass", "sp":
                        result.serverPass = val

                    of "port", "p":
                        result.port = parseInt(val)

                    of "user", "u":
                        result.username = val

                    of "nick", "n":
                        result.nickname = val

                    of "pass", "pa":
                        result.password = val

                    of "channels", "c":
                        result.channels = split(val, ",")

                    of "verbosity", "v":
                        result.verbosity = parseInt(val)

                    of "config", "co":
                        result.configFile = val

            of cmdEnd:
                discard

    for file in @["~/.billrc", "~/.config/BillsPC/default.config", "bill.config", result.configFile]:
        process(file, result)

proc print*(info: connection) =
    echo("Server: " & info.server & "\n" &
         "Server Password: " & info.serverPass & "\n" &
         "Port: " & $info.port & "\n" &
         "Username: " & info.username & "\n" &
         "Nickname: " & info.nickname & "\n" &
         "Password: " & info.password & "\n" &
         "Channels: " & $info.channels & "\n" &
         "Verbosity: " & $info.verbosity & "\n" &
         "Configuration File: " & info.configFile & "\n"
    )